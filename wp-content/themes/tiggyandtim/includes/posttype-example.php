<?php
function create_example_posttype(){
	$args = array(
		'label' => 'Example',
		'labels' => array(
			'name' => __('Example'),
			'singular_name' => __('Example'),
			'add_new' => __('Add New'),
			'add_new_item' => __('Add New Example'),
			'edit_item' => __('Edit Example'),
			'new_item' => __('New Example'),
			'view_item' => __('View Example'),
			'search_items' => __('Search Examples'),
			'not_found' => __('No Examples found'),
			'not_found_in_trash' => __('No Examples found in trash'),
			'all_items' => __('All Examples'),
			'archives' => __('Example Archives'),
			'insert_into_item' => __('Insert into Example'),
			'uploaded_to_this_item' => __('Uploaded to this Example'),
			'featured_image' => __('Example Picture'),
			'set_featured_image' => __('Set Example Picture'),
			'remove_featured_image' => __('Remove Example Picture'),
			'use_featured_image' => __('Use Example Picture'),
			'menu_name' => __('Examples')
		),
		'description' => __('Post type to contain content for the example.'),
		'public' => false,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 18,
		'menu_icon' => 'dashicons-images-alt',
		'supports' => array('title','editor','revisions','thumbnail','page-attributes'),
		'has_archive' => true,
		'rewrite' => array('slug'=>'examples','with_front'=>false)
	);
	register_post_type('example',$args);
}
add_action('init','create_example_posttype',0);
?>