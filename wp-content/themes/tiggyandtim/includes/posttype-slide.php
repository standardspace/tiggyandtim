<?php
function create_slide_posttype(){
	$args = array(
		'label' => 'Slide',
		'labels' => array(
			'name' => __('Slide'),
			'singular_name' => __('Slide'),
			'add_new' => __('Add New'),
			'add_new_item' => __('Add New Slide'),
			'edit_item' => __('Edit Slide'),
			'new_item' => __('New Slide'),
			'view_item' => __('View Slide'),
			'search_items' => __('Search Slides'),
			'not_found' => __('No Slides found'),
			'not_found_in_trash' => __('No Slides found in trash'),
			'all_items' => __('All Slides'),
			'archives' => __('Slide Archives'),
			'insert_into_item' => __('Insert into Slide'),
			'uploaded_to_this_item' => __('Uploaded to this Slide'),
			'featured_image' => __('Slide Picture'),
			'set_featured_image' => __('Set Slide Picture'),
			'remove_featured_image' => __('Remove Slide Picture'),
			'use_featured_image' => __('Use Slide Picture'),
			'menu_name' => __('Homepage Slides')
		),
		'description' => __('Post type to contain content for the homepage slides post type.'),
		'public' => false,
		'exclude_from_search' => true,
		'publicly_queryable' => false,
		'show_ui' => true,
		'show_in_nav_menus' => true,
		'show_in_menu' => true,
		'show_in_admin_bar' => true,
		'menu_position' => 21,
		'menu_icon' => 'dashicons-images-alt2',
		'supports' => array('title','thumbnail','page-attributes')
	);
	register_post_type('slide',$args);
}
add_action('init','create_slide_posttype',0);
?>