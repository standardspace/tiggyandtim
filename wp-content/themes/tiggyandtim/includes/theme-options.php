<?php
add_action('admin_init','custom_theme_options');
function custom_theme_options() {
	$saved_settings = get_option(ot_settings_id(),array());
	$custom_settings = array( 
		'contextual_help' => array( 
			'content'       => array( 
				array(
					'id'        => 'option_types_help',
					'title'     => __('Option Types','option-tree-theme'),
					'content'   => '<p>'.__('Help content goes here!','option-tree-theme').'</p>'
				)
			),
			'sidebar'       => '<p>'.__('Sidebar content goes here!', 'option-tree-theme').'</p>'
		),
		'sections'        => array(
			array(
				'id'          => 'header',
				'title'       => __('Header Option','option-tree-theme')
			), 
			array(
				'id'          => 'footer',
				'title'       => __('Footer Option','option-tree-theme')
			),
		),
		'settings'        => array( 
			array(
				'id'          => 'logo_header',
				'label'       => __( 'Header Logo', 'option-tree-theme' ),
				'std'         => get_template_directory_uri().'/img/logo.png',
				'type'        => 'upload',
				'section'     => 'header',
				'rows'        => '4',
				'min_max_step'=> '',
				'class'       => '',
				'condition'   => '',
				'operator'    => 'and'
			),
		)
	);
	$custom_settings = apply_filters(ot_settings_id().'_args',$custom_setting );
	if($saved_settings !== $custom_settings){
		update_option(ot_settings_id(),$custom_settings); 
	}
}
?>