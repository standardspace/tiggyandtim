<?php
add_action('admin_init','homepage_meta_boxes');
function homepage_meta_boxes() {
	$args = array(
		'id'          => 'homepage_data_box',
		'title'       => 'Favourite Products',
		'desc'        => '',
		'pages'       => array('page'),
		'context'     => 'normal',
		'priority'    => 'low',
		'fields'      => array(
			array(
				'id'        => 'homepage_product_1',
				'label'		=> '',
				'type'		=> 'custom-post-type-select',
				'std'		=> '',
				'post_type' => 'product',
			),
			array(
				'id'        => 'homepage_product_2',
				'label'		=> '',
				'type'		=> 'custom-post-type-select',
				'std'		=> '',
				'post_type' => 'product',
			),
			array(
				'id'        => 'homepage_product_3',
				'label'		=> '',
				'type'		=> 'custom-post-type-select',
				'std'		=> '',
				'post_type' => 'product',
			),
			array(
				'id'        => 'homepage_product_4',
				'label'		=> '',
				'type'		=> 'custom-post-type-select',
				'std'		=> '',
				'post_type' => 'product',
			),
			array(
				'id'        => 'homepage_product_5',
				'label'		=> '',
				'type'		=> 'custom-post-type-select',
				'std'		=> '',
				'post_type' => 'product',
			),
			array(
				'id'        => 'homepage_product_6',
				'label'		=> '',
				'type'		=> 'custom-post-type-select',
				'std'		=> '',
				'post_type' => 'product',
			),
			array(
				'id'        => 'homepage_product_7',
				'label'		=> '',
				'type'		=> 'custom-post-type-select',
				'std'		=> '',
				'post_type' => 'product',
			),
			array(
				'id'        => 'homepage_product_8',
				'label'		=> '',
				'type'		=> 'custom-post-type-select',
				'std'		=> '',
				'post_type' => 'product',
			),
		)
	);
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
	if($template_file == 'template-homepage.php'){
		ot_register_meta_box($args);
	}
}
?>