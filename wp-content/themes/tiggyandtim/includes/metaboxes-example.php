<?php
add_action('admin_init','example_meta_boxes');
function example_meta_boxes() {
	$args = array(
		'id'          => 'example_data_box',
		'title'       => 'Example Descriptions',
		'desc'        => '',
		'pages'       => array('page'),
		'context'     => 'normal',
		'priority'    => 'low',
		'fields'      => array(
			array(
				'id'          => 'example_textarea',
				'label'       => 'Example Description',
				'type'        => 'textarea',
				'std'         => '',
			),
		)
	);
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
	$template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
	if($template_file == 'template-services.php'){
		ot_register_meta_box($args);
	}
}
?>