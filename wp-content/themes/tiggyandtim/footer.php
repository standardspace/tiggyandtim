	<section id="newsletter-signup">
		<div class="sizer">
			<div class="content">
				<form action="http://artbyartists.createsend.com/t/r/s/kulhljh/" method="post" id="subForm">
					<input id="fieldEmail" name="cm-kulhljh-kulhljh" type="email" placeholder="enter email to sign up" required />
					<button class="newsletter-go" type="submit">go</button>
				</form>
			</div>
		</div>
	</section>
	<footer>
		<ul class="social">
			<li><a href="https://www.facebook.com/tiggyandtim" target="_blank"><i class="fa fa-facebook"></i></a></li>
			<li><a href="https://www.instagram.com/tiggyandtim/" target="_blank"><i class="fa fa-instagram"></i></a></li>
			<li><a href="https://twitter.com/tiggyandtim" target="_blank"><i class="fa fa-twitter"></i></a></li>
		</ul>
		<nav class="footer-menu"><?php wp_nav_menu(array('theme_location'=>'footer-menu','container'=>false)); ?></nav>
	</footer>
	<section id="aba-footer">
		<a id="aba-logo" href="http://artbyartists.co.uk" target="_blank" title="Proudly powered by Art By Artists">
			<img src="/wp-content/uploads/2016/12/aba-logo.png" alt="Proudly powered by Art By Artists" width="290" height="168" />
			<span>POWERED BY ARTBYARTISTS.CO.UK</span>
		</a>
	</section>
	<!-- WP Footer -->
	<?php wp_footer(); ?>
</body>
</html>