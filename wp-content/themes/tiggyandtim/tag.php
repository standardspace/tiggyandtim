<?php get_header(); ?>
<?php while(have_posts()) : the_post(); ?>
	<h1><?php printf(single_tag_title('',false)); ?></h1>
	<?php the_content(); ?>
<?php endwhile; ?>
<?php get_footer(); ?>