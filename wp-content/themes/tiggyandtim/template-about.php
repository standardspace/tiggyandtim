<?php
/*
Template Name: About
*/
?>
<?php get_header(); ?>
<?php
	if (have_posts()) :
	while(have_posts()) : the_post();
?>
	<div class="sizer">
		<div class="content">
			<?php the_content(); ?>
		</div>
		<div class="our-products">
			<h2>our products</h2>
			<ul class="product-category-boxes">
				<?php
					$args = array(
							 'taxonomy'     => 'product_cat',
							 'orderby'      => 'name',
							 'show_count'   => 0,
							 'pad_counts'   => 0,
							 'hierarchical' => 0,
							 'parent' 		=> 0
					  );
					 $all_categories = get_categories( $args );
					 foreach($all_categories as $cat){
						 //var_dump($cat);
						 $thumbnailID = get_woocommerce_term_meta($cat->term_id,'thumbnail_id',true);
						 $image = wp_get_attachment_url($thumbnailID); 
						 echo '<li class="product-category-box">';
						 echo '<a href="/product-category/'.$cat->slug.'/" class="image-container" style="background-image:url(\''.$image.'\')"></a>';
						 echo '<h3>'.$cat->name.'</h3>';
						 echo '<p>'.$cat->description.'</p>';
						 echo '<span class="view-link"><a href="/product-category/'.$cat->slug.'/">view</a></span>';
						 echo '</li>';
					 }
				?>
			</ul>
		</div>
	</div>
<?php
	endwhile;
else :
	echo '<p>Sorry but it would seem something has gone wrong.</p>';
endif;
?>	
<?php get_footer(); ?>