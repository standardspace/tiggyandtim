<?php get_header(); ?>
<?php
	if (have_posts()) :
	while(have_posts()) : the_post();
?>
	<div class="sizer">
		<div class="content">
			<?php the_content(); ?>
		</div>
	</div>
<?php
	endwhile;
else :
	echo '<p>Sorry but it would seem something has gone wrong.</p>';
endif;
?>	
<?php get_footer(); ?>