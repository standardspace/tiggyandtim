<?php
	/* ---------
	   GENERAL
	----------*/
	// Thumbnail Support
	add_theme_support('post-thumbnails',array('post','page','slide','product'));
	// Menus
    function register_my_menus() {
        register_nav_menu('main-menu',__('Main Menu'));
		register_nav_menu('footer-menu',__('Footer Menu'));
    }
    add_action('init', 'register_my_menus');
	
	/* ------------------
	   REGISTER PLUGINS
	-------------------*/
	add_filter('ot_theme_mode','__return_true');
	add_filter('ot_show_pages','__return_false');
	add_filter('ot_child_theme_mode','__return_false');
	require_once('plugins/option-tree/ot-loader.php');
	
	/* -------------------------
	   REGISTER STYLES/SCRIPTS
	--------------------------*/
	add_action('wp_enqueue_scripts','register_theme_components' );
	function register_theme_components() {
		wp_enqueue_style('general-styling',get_template_directory_uri().'/style.css');
		wp_enqueue_style('font-awesome',get_template_directory_uri().'/css/font-awesome.min.css');
		wp_enqueue_script('slider-script',get_template_directory_uri().'/js/slider.js',array('jquery'),'1.0',true);
		wp_enqueue_script('general-script',get_template_directory_uri().'/js/general.js',array('jquery'),'1.0',true);
	}
	
	/* -------------------
	   CUSTOM POST TYPES
	--------------------*/
	require_once('includes/posttype-slide.php');
	
	/* -------------------
	   CUSTOM META BOXES
	--------------------*/
	require_once('includes/metaboxes-homepage.php');
	
	/* ---------------
	   THEME OPTIONS
	----------------*/
	//require_once('includes/theme-options.php');
	
	/* ---------------------------
	   CHANGE TITLE PLACEHOLDER
	----------------------------*/
	function wpb_change_title_text($title){
		 $screen = get_current_screen();
		 if('banner' == $screen->post_type){
			  //$title = 'Banner Name';
		 }
		 return $title;
	}
	add_filter('enter_title_here','wpb_change_title_text');
	
	/* ---------------------------
	     CUSTOM POST MENU FIX
	----------------------------*/
	add_action('nav_menu_css_class','add_current_nav_class',10,2);
	function add_current_nav_class($classes, $item) {
		global $post;
		$current_post_type = get_post_type_object(get_post_type($post->ID));
		$current_post_type_slug = $current_post_type->rewrite[slug];
		$menu_slug = strtolower(trim($item->url));
		if(strpos($menu_slug,$current_post_type_slug)!==false) {
			$classes[] = 'current-menu-item';
		}
		return $classes;
	}

	/* ---------------------------
	         MISC FUNCTIONS
	----------------------------*/
	function remove_menus(){
		remove_menu_page('edit.php');                   //Posts
		remove_menu_page('edit-comments.php');          //Comments
		remove_menu_page('tools.php');                  //Tools
	}
	add_action('admin_menu','remove_menus');

	/* ---------------------------
	         WOO FUNCTIONS
	----------------------------*/ 
	
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 25 );
	
	function my_header_add_to_cart_fragment( $fragments ) {
 
		ob_start();
		$count = WC()->cart->cart_contents_count;
		?><a class="basket" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/basket.png" alt="My basket"><span>My basket</span>
		<?php
		if ( $count > 0 ) {
			?>
			<span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
			<?php            
		}
			?></a><?php

		$fragments['a.basket'] = ob_get_clean();

		return $fragments;
	}
	add_filter( 'woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment' );
function woocommerce_subcats_from_parentcat_by_ID($parent_cat_ID) {
	
	$parentCat = get_term($parent_cat_ID,'product_cat');
	$parentInfo = get_category($parent_cat_ID);
	$parentCatName = $parentCat->term_name;
	echo '<ul class="cats-list">';
	if( $parentInfo->parent > 0){
		$args = array(
			'hierarchical' => 1,
			'show_option_none' => '',
			'hide_empty' => 0,
			'parent' => $parentInfo->parent,
			'taxonomy' => 'product_cat'
		);
		$subcats = get_categories($args);
		echo '<li><a href="'.get_category_link($parentInfo->parent).'">All</a></li>';
		foreach ($subcats as $sc) {
			$subcats = get_categories($args);
			$link = get_term_link( $sc->slug, $sc->taxonomy );
			echo '<li><a href="'. $link .'">'.$sc->name.'</a></li>';
		}
	}else{
		$args = array(
			'hierarchical' => 1,
			'show_option_none' => '',
			'hide_empty' => 0,
			'parent' => $parent_cat_ID,
			'taxonomy' => 'product_cat'
		);
		$subcats = get_categories($args);

		echo '<li class="' , (is_shop() ? 'current' : '') .'"><a href="'.get_category_link($parent_cat_ID).'">All</a></li>';

		foreach ($subcats as $sc) {
			$link = get_term_link( $sc->slug, $sc->taxonomy );
			echo '<li><a href="'. $link .'">'.$sc->name.'</a></li>';
		}
	}
	echo '</ul>';
}
function woocommerce_subcats_from_parentcat_by_NAME($parent_cat_NAME) {
	$IDbyNAME = get_term_by('name', $parent_cat_NAME, 'product_cat');
	$product_cat_ID = $IDbyNAME->term_id;
   	$args = array(
       'hierarchical' => 1,
       'show_option_none' => '',
       'hide_empty' => 0,
       'parent' => $product_cat_ID,
       'taxonomy' => 'product_cat'
	);
	$subcats = get_categories($args);
	echo '<ul class="wooc_sclist">';
	foreach ($subcats as $sc) {
        $link = get_term_link( $sc->slug, $sc->taxonomy );
		echo '<li><a href="'. $link .'">'.$sc->name.'</a></li>';
    }
	echo '</ul>';
}

// Woocommerce - Create form placeholders based on Label texts
add_filter( 'woocommerce_checkout_fields', 'placeholder_checkout_fields' );

function placeholder_checkout_fields( $fields ) {

	// Special Exceptions
	$exceptions = array(
		'billing_address_1' => 'Street Address',
		'billing_address_2' => 'Apartment, suite, unit etc. (optional)',
		'shipping_address_1' => 'Street Address',
		'shipping_address_2' => 'Apartment, suite, unit etc. (optional)',
		'order_comments' => 'Notes about your order such as special delivery requirements'
	);

	foreach ( $fields as &$field_section ) {
		foreach ( $field_section as $field_name => &$field ) {

			// Check for exceptions
			if ( array_key_exists( $field_name, $exceptions ) ) {
				$field['placeholder'] = $exceptions[$field_name];
			}
			else {
				$field['placeholder'] = $field['label'];
			}

		}
	}

	return $fields;
}