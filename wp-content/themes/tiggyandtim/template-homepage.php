<?php
/*
Template Name: Homepage
*/
?>
<?php get_header(); ?>
<?php
	if (have_posts()) :
	while(have_posts()) : the_post();
?>
	<div class="sizer">
		<div class="slider">
			<?php
				$slideData = array(
					'post_type' => array('slide'),
					'posts_per_page' => -1,
					'orderby' => 'menu_order',
					'order' => 'ASC',
				);
				query_posts($slideData);
				while(have_posts()) : the_post();
					$thumb = wp_get_attachment_image_src(get_post_thumbnail_id(),'full');	
			?>
			<div class="slide" style="background-image:url('<?php echo $thumb[0]; ?>');"></div>
			<?php
				endwhile;
				wp_reset_query();
			?>
			<a href="#" class="prev"></a>
			<a href="#" class="next"></a>
			<h2 class="slide-badge">
				Free <span class="slide-badge-sub">postage &amp; packaging<span>
			</h2>
		</div>
		<div class="content">
			<?php the_content(); ?>
			<div class="more-link"><a href="/about/">more</a></div>
		</div>
		<div class="favourites">
			<h2>our favourites</h2>
			<ul class="product-boxes">
			<?php
				for($i=1;$i<=8;$i++){
					$prodID = get_post_meta(get_the_ID(),'homepage_product_'.$i,true);
					if($prodID){
						$thumb = wp_get_attachment_image_src(get_post_thumbnail_id($prodID),'thumbnail');
						$product = wc_get_product($prodID);
						echo '<li class="product-box">';
						echo '<a href="'.get_the_permalink($prodID).'" class="image-container" style="background-image:url(\''.$thumb[0].'\')"></a>';
						echo '<h3>'.get_the_title($prodID).'</h3>';
						echo '<span class="price">&pound;'.$product->get_price().' each</span>';
						echo '<a href="'.do_shortcode('[add_to_cart_url id="'.$prodID.'"]').'" class="button">' , ($product->is_type( 'variable' ) ? 'Select options' : 'Add to cart') . '</a>';
						echo '</li>';
					}
				}
			?>
			</ul>
			<div class="more-link"><a href="/shop/">shop</a></div>
		</div>
	</div>
<?php
	endwhile;
else :
	echo '<p>Sorry but it would seem something has gone wrong.</p>';
endif;
?>	
<?php get_footer(); ?>