<?php get_header(); ?>
	<h1>Error - 404</h1>
	<p>There seems to have been an error. Please navigate to the <a href="<?php echo esc_url(home_url('/')); ?>">Home Page</a> or use the menu to find what you are looking for.</p>
<?php get_footer(); ?>