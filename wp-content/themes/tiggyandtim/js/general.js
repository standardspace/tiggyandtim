jQuery(function($){
	$('.main-menu > ul > li > a').click(function(e){
		var clickedli = $(this).parent('li');
		if(clickedli.children("ul").length){
			$('.main-menu > ul > li.active > ul').slideUp('fast');
			if(clickedli.hasClass('active')){
				$('.main-menu > ul > li > ul').slideUp('fast',function(){
					$('.main-menu > ul > li').removeClass('active');
				});
			}else{
				$('.main-menu > ul > li').removeClass('active');
				clickedli.addClass('active');
				clickedli.children('ul').slideDown();
			}
			e.preventDefault();
		}
	});
	
	$('.burger-menu, .close-menu').click(function(){
		if($('.main-menu').hasClass('active')){
			$('.main-menu').removeClass('active');
		}else{
			$('.main-menu').addClass('active');
		}
	});
	
	var $catURL = window.location.href;
	$('.cats-list li a').filter(function(){
		return $(this).attr('href') === $catURL;
	}).parent('li').addClass('current');
	
});