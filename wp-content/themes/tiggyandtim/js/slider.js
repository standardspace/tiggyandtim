var slides, slideWidth;
var currSlide = 1;
jQuery(function($){
	"use strict";
	slides = $('.slider .slide').length;
	slideWidth = $('.slider .slide').width();
	
	function moveRight(){
		slideWidth = $('.slider .slide').width();
		var nextSlide = currSlide + 1;
		if(nextSlide > slides){nextSlide = 1;}
		$('.slider .slide:nth-child('+currSlide+')').css({'z-index':0});
		$('.slider .slide:nth-child('+nextSlide+')').css({'left':slideWidth,'display':'block','z-index':1});
		$('.slider .slide:nth-child('+nextSlide+')').animate({'left':0},1000,'swing');
		currSlide = nextSlide;
	}
	
	function moveLeft(){
		slideWidth = $('.slider .slide').width();
		var nextSlide = currSlide - 1;
		if(nextSlide < 1){nextSlide = slides;}
		$('.slider .slide:nth-child('+currSlide+')').css({'z-index':0});
		$('.slider .slide:nth-child('+nextSlide+')').css({'left':-slideWidth,'display':'block','z-index':1});
		$('.slider .slide:nth-child('+nextSlide+')').animate({'left':0},1000,'swing');
		currSlide = nextSlide;
	}
	
	$('.slider .next').click(function(e){
		moveRight();
		e.preventDefault();
	});
	
	$('.slider .prev').click(function(e){
		moveLeft();
		e.preventDefault();
	});
	
	if(slides > 1){
		setInterval(function(){moveRight();},10000);
	}else{
		$('.slider .next, .slider .prev').hide();
	}
});