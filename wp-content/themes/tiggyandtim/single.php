<?php get_header(); ?>
<?php
	if (have_posts()) :
	while(have_posts()) : the_post();
?>
	<h1><?php the_title(); ?></h1>
	<?php the_content(); ?>
<?php
	endwhile;
else :
	echo '<p>Sorry but it would seem something has gone wrong.</p>';
endif;
?>	
<?php get_footer(); ?>